import React, { useEffect, useState } from "react";
import axios from 'axios';

// components
import { API_URL, AUTH_TOKEN } from "APIURL";

import CardStats from "components/Cards/CardStats.js";

export default function Dashboard(props) {

  const [Api, SetApi] = useState([])

  console.log("DATA ", Api)
  useEffect(() => {

    let config = {
      headers: {
        'Authorization': localStorage.getItem("tokendata")
      }
    }
    axios.get(`${API_URL}admin/dashboard`, config)
      .then(function (response) {
        // handle success
        console.log("API", response.data);
        SetApi(response.data);
        console.log("NEW", Api)
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });

  }, []);

  return (
    <>
      <div className="relative dashboard-main">
        <div className="title-main">
          <h1>Applications to Review</h1>
          <p>Update approximately every 1 mins. Data as of 4:05 pm</p>
        </div>
        <div className="mx-auto w-full">
          <div>
            {/* Card stats */}
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 xl:w-3/12 pr-4 dashboard-box" onClick={() => props.history.push("/admin/settings/unReviewed")}>
                <CardStats
                  statSubtitle="Unreviewed"
                  statTitle={Api.unReviewedUsersCount}
                />
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4 dashboard-box" onClick={() => props.history.push("/admin/settings/reSubmitted")}>
                <CardStats
                  statSubtitle="Applicant resubmitted"
                  statTitle={Api.resubmittedUsersCount}

                />
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4 dashboard-box" onClick={() => props.history.push("/admin/settings/pendingSmartMeterInstallation")}>
                <CardStats
                  statSubtitle="Pending install"
                  statTitle={Api.pendingSmartMeterInstallationUsersCount}

                />
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 pl-4 dashboard-box" onClick={() => props.history.push("/admin/settings/pendingTransferAgreement")}>
                <CardStats
                  statSubtitle="Pending review by TP"
                  statTitle={Api.pendingTransferAgreementUsersCount}

                />
              </div>
            </div>
          </div>
          <div className="title-main" style={{ marginTop: 40 }}>
            <h1>Activated Customers</h1>
            <p>Update approximately every 1 mins. Data as of 4:05 pm</p>
          </div>
          <div>


            {/* Card stats */}
            <div className="flex flex-wrap" >
              <div className="w-full lg:w-6/12 xl:w-3/12 pr-4 dashboard-box">
                <CardStats
                  statSubtitle="Service suspended"
                  statTitle={Api.serviceSuspendedUsersCount}

                />
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4 dashboard-box">
                <CardStats
                  statSubtitle="Unpaid"
                  statTitle={Api.unPaidUsersCount}

                />
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4 dashboard-box">
                <CardStats
                  statSubtitle="Payment failed"
                  statTitle={Api.paymentFailedUsersCount}

                />
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 pl-4 dashboard-box">
                <CardStats
                  statSubtitle="Overdue payment 30days"
                  statTitle={Api.paymentOverdueUsersCount}

                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
