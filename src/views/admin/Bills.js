import React,{useState} from 'react';
import { Link } from 'react-router-dom';


export default function Bills(props,color) {
    let Data=[];
    //Data = props?.getData;
    Data.push(props?.getData);
    console.log("MyData",Data[0]?.bills)

    const openInNewTab = (url) => {
        const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
        if (newWindow) newWindow.opener = null
      }
      console.log("data --->", Data[0]?.bills[0].kiwiUsage);

    
function download_csv_file() {
  var data =
    "Bill Month,Usage Date,Charge,KiWi Usage,Payment Status,Payment Method,Pdf\n";
  for (var i = 0; i < Data[0]?.bills.length; i++) {
    console.log("->", Data[0]?.bills.length);
    console.log("->", Data[0]?.bills[i].billMonth.year);
    data +=
      Data[0].bills[i].billMonth.year +
      "/" +
      Data[0].bills[i].billMonth.month +
      "," +
      Data[0].bills[i].usageFromDate.month +
      "/" +
      Data[0].bills[i].usageFromDate.day +
      " - " +
      Data[0].bills[i].usageToDate.month +
      "/" +
      Data[0].bills[i].usageToDate.day +
      "," +
      Data[0].bills[i].kiwiUsage * 5.5 +
      "," +
      Data[0].bills[i].kiwiUsage +
      "," +
      Data[0].bills[i].paymentStatus +
      "," +
      Data[0].bills[i].paymentMethod +
      "," +
      Data[0].bills[i].pdfLink;
    data += "\n";
  }
  var hiddenElement = document.createElement("a");
  hiddenElement.href = "data:text/csv;charset=utf-8," + encodeURI(data);
  hiddenElement.target = "_blank";
  hiddenElement.download = "ApplicationReview.csv";
  hiddenElement.click();
}
    return (
        <div className="block w-full overflow-x-auto">
          <div className="w-full px-4 flex-1">
        <div className="right-filter">
          <div className="export-button">
            <button
              onClick={() => {
                download_csv_file();
              }}
            >
              Export to Csv
            </button>
          </div>
        </div>
      </div>
        {/* Projects table */}
        <table className="items-center w-full bg-transparent border-collapse">
          <thead>
            <tr>
              <th
                className={
                  "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                  (color === "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                }
              >
                Bill month
              </th>
              <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
               Usage date
              </th>
              <th
                className={
                  "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                  (color === "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                }
              >
               Charge
              </th>
              <th
                className={
                  "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                  (color === "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                }
              >
               KiWi usage (kWh)
              </th>
              <th
                className={
                  "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                  (color === "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                }
              >
                Payment status
              </th>
              <th
                className={
                  "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                  (color === "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                }
              >
                Payment method
              </th>
              <th
                className={
                  "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                  (color === "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                }
              >
               pdf
              </th>
            </tr>
          </thead>
          <tbody>
            {Data[0]?.bills?.map((item,key) => (
              <tr style={{ cursor: "pointer" }}>
                <th  key={key}className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left flex items-center">
              
                  <span
                    className={
                      "ml-3 font-bold " +
                      +(color ===  "text-white")
                    }
                  >
                    {item.billMonth.year}/{item.billMonth.month}
                  </span>
                 
                </th>
                <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                
                {item.usageFromDate.month}/{item.usageFromDate.day} - {item.usageToDate.month}/{item.usageToDate.day}
               
                </td>
                <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
               
                {item.kiwiUsage * 5.5}
               
                </td>
                <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
               
                {item.kiwiUsage}
               
                </td>
                <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                  <div className="flex">
                 
                    {item.paymentStatus}
                  
                  </div>
                </td>
                <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                  <div className="flex items-center">
                 
                    {/* <span className="mr-2">{item.customerStatus}</span> */}
                    {item.paymentMethod}
                 
                  </div>
                </td>
                <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-right">
               
                <button style={{color:'#1890FF',textDecoration:'underline'}} onClick={() =>window.open(`https://${item.pdfLink}/`, "_blank")}>View</button>
               
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
}
