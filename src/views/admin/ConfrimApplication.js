import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import { useParams } from "react-router";
import { popper } from "@popperjs/core";
import Popup from "./Popup";
import { height } from "tailwindcss/defaultTheme";
import { API_URL } from "APIURL";
import Spinner from "./Spinner";

export default function ConfirmApplication(props) {
  const [Data, SetData] = useState();

  const [isOpen, setIsOpen] = useState(false);
  const [emailOpen, setEmailOpen] = useState(false);
  const [signOpen, setSignOpen] = useState(false);
  const [householdName, setHouseholdName] = useState(false);
  const [taipowerOpen, setTaiPowerOpen] = useState(false);
  const [primaryOpen, setPrimaryOpen] = useState(false);
  const [taiAddressOpen, setTaiAddressOpen] = useState(false);
  const [mailingOpen, setMailingOpen] = useState(false);
  const [emailChange, setEmailChange] = useState(undefined);
  const [taxChange, setTaxChange] = useState(undefined);
  const [householdNameChange, setHouseholdNameChange] = useState(undefined);
  const [taiAccountChages, setTaiAccounntChanges] = useState(undefined);
  const [primaryIdChange, setPrimaryIdChange] = useState(undefined);
  const [taiAddressChanges, setTaiAddressChanges] = useState(undefined);
  const [mailingAddressChange, setMailingAddressChange] = useState(undefined);
  const [textArea, setTextArea] = useState();
  const [MyArray, SetArrays] = useState([]);
  const [RejectOpen, setRejectOpen] = useState(false);
  const [payment, setPayment] = useState(false);
  const [invoice, setInvoice] = useState(false);
  const [loading, setLoading] = useState(false);
  const [taxOpen, setTaxOpen] = useState(false);

  const handleEditEmail = (lable, value) => {
    console.log("lable ---->", lable, value);
    axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: { userEmail: value },
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/reviewusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setEmailOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditHousehold = (lable, value) => {
    console.log("lable ---->", lable, value);
    axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: { taipowerAccountName: value },
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/reviewusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setHouseholdName(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditTaiAccountNumber = (lable, value) => {
    console.log("lable ---->", lable, value);
    axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: { taipowerAccountNumber: value },
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/reviewusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setTaiPowerOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditPrimaryId = (lable, value) => {
    console.log("lable ---->", lable, value);
    axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: {"customerId":{
        "customerIdType": 1,
        "customerIdValue": value}}
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/reviewusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setPrimaryOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditTaiAddress = (lable, value) => {
    console.log("lable ---->", lable, value);
    axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: {userAddress: value },
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/reviewusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setTaiAddressOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditMailingAddress = (lable, value) => {
    console.log("lable ---->", lable, value);
    axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: { billingAddress: value },
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/reviewusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setMailingOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditTax = (lable, value) => {
    console.log("value ----->", value);
    axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: {"customerId":{
        "customerIdType": 2,
        "customerIdValue": value}}
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/reviewusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setTaxOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  }

  const activetedService = async () => {
    setLoading(true)
    //loder true
    await axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/activatekiwinewenergy/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") }
    })
    .then(async function (response) {
      setLoading(false);
      props.history.push("/admin/settings/all");
      //loader false
    })
    .catch(function (error) {
      console.log(error);
    })
    .then(function () {
      // always executed
    });       
  }

  const yesService = async() => {
    await axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/smartmeterinstalled/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") }
    })
    .then(async function (response) {
      console.log("res ------->", response.data.docs)
    })
    .catch(function (error) {
      console.log(error);
    })
    .then(function () {
      // always executed
    });

    let config = {
      headers: {
        Authorization: localStorage.getItem("tokendata"),
      },
    };
    axios
      .get(
        `${API_URL}admin/reviewusers/${id}`,
        config
      )

      .then(async function (response) {
        // handle success
        console.log("API", response.data);
        await SetData(response.data);
        console.log(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }

  const approveService = async() => {
    console.log("1s")
    await axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/verificationsuccess/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") }
    })
    .then(async function (response) {
      console.log("res ------->", response.data.docs)
    })
    .catch(function (error) {
      console.log(error);
    })
    .then(function () {
      // always executed
    });

    let config = {
      headers: {
        Authorization: localStorage.getItem("tokendata"),
      },
    };
    axios
      .get(
        `${API_URL}admin/reviewusers/${id}`,
        config
      )

      .then(async function (response) {
        // handle success
        console.log("API", response.data);
        await SetData(response.data);
        console.log(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }


  const { id } = useParams();
  //console.log(id);
  // Pass useEffect a function
  useEffect(() => {
    let config = {
      headers: {
        Authorization: localStorage.getItem("tokendata"),
      },
    };
    axios
      .get(
        `http://kiwiadmindashboardv2staging-env.eba-vr3jhvkc.us-east-2.elasticbeanstalk.com/admin/reviewusers/${id}`,
        config
      )

      .then(async function (response) {
        // handle success
        console.log("API", response.data);
        await SetData(response.data);
        console.log(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });

    //return () => console.log('unmounting...');
  }, [Data]);

  const handleInputChange = async(event) => {
    const target = event.target;
    var value = target.value;
    console.log("value",value) 
   // var index = checkboxValue.indexOf(value); 
    var index2= MyArray.indexOf(value); 
    //let updatedCheckbox = await checkboxValue?.map((item) => item !== value ? checkboxValue.push(value) : item ) 
    //checkboxValue.indexOf(value) === -1 ? checkboxValue.push(value) : checkboxValue.splice(index, 1);
    MyArray.indexOf(value) === -1 ? MyArray.push(value) : MyArray.splice(index2, 1);
    
    //const items = this.state.items.filter(item => item.id !== itemId);
    //this.setState({ items: items });
    //console.log(checkboxValue)
  }

  const onFinal = () =>
  {
    //const textArea = e.target.value;
    //setTextArea(textArea);
    console.log("hye",textArea)
    //checkboxValue.push(textArea);
    MyArray.push(textArea);
    
    console.log("SetArr",MyArray);
    let myBody = MyArray.toString();
    console.log("Check",myBody);

    axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/verificationfailed/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: { rejectedAdminResponse: myBody },
    }).then((res) => {
      console.log("res",res);
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(
          `${API_URL}admin/reviewusers/${id}`,
          config
        )
  
        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          await setRejectOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
  }) 
}

  return (
    <div className="application-to-review-main">
      <div className="title-main breadcrum">
        <h1>Applications to Review - <span>顏健康</span></h1>
      </div>
      {loading && <div className="spinner-loader"><Spinner /></div>}
      <div className="w-full">
        <div className="flex flex-wrap">
          <div className="w-full pr-4 flex-1">
            {/* First block  */}
            <div className="application-info-box">
              <h2>Applicant info</h2>
              {console.log("data ----->", Data)}
              <ul>
                <li> <span> Date of Application </span> <span> {Data?.createdAt} </span> </li>
                <li> <span> Status </span> <span> <span> {Data?.customerStatus} </span> </span> </li>
                <li> <span> Contact Name </span> <span> {Data?.userName} </span> </li>
                <li> <span> Contact Email </span> <span> {Data?.userEmail} <button className="btn" onClick={() => setEmailOpen(true)}>Edit</button> </span> </li>
                <li> <span> Contact Mobile Phone </span> <span> {Data?.userPhone} </span> </li>
                <li> <span> E-sign </span> <span> <button className="btn" onClick={() => { setSignOpen(true); }}>View</button> </span> </li>
              </ul>
            </div>
            {/* second Block */}
            <div className="application-info-box">
              <h2>KiWi Plan</h2>
              <ul>
                <li> <span> KiWi’s Plan 35 kwh/month </span> <span> {Data?.kiwiplan} </span> </li>
                <li> <span> Promo / Referral code </span> <span> {Data?.promoCode} </span> </li>
                <li> <span> Contact Name </span> <span> {Data?.userName} </span> </li>
              </ul>
            </div>
            {/* Third block  */}
            <div className="application-info-box">
              <h2>TaiPower info</h2>
              <ul>
                <li> <span> Household Name </span> <span> {Data?.taipowerAccountName} <button className="btn" onClick={() => setHouseholdName(true)}> Edit </button> </span> </li>
                <li> <span> TaiPower account number </span> <span> {Data?.taipowerAccountNumber} <button className="btn" onClick={() => setTaiPowerOpen(true)}> Edit </button> </span> </li>
                <li> <span> Pirmary ID </span> <span> {Data?.customerId} <button className="btn" onClick={() => setPrimaryOpen(true)}> Edit </button> </span> </li>
                <li> <span> Tax ID </span> <span> {Data?.taxId} <button className="btn" onClick={() => setTaxOpen(true)}> Edit </button>  </span> </li>
                <li> <span> TaiPower account number address </span> <span> {Data?.userAddress} <button className="btn" onClick={() => setTaiAddressOpen(true)}> Edit </button> </span> </li>
                <li> <span> Mailing address </span> <span>{Data?.billingAddress}<button className="btn" onClick={() => setMailingOpen(true)} > Edit </button></span> </li>
              </ul>
            </div>
            {/* Fourth Block    */}
            <div className="application-info-box">
              <h2>Payment</h2>
              <ul style={{cursor: "pointer"}} onClick={() => setPayment(true)}>
                <li> <span> Credit card (One-time) </span> <span> {`**** **** **** ${Data?.paymentDetails}`} </span> </li>
              </ul>
            </div>
            {/* Fifth Block   */}
            <div className="application-info-box">
              <h2>E-Invoice</h2>
              <ul style={{cursor: "pointer"}} onClick={() => setInvoice(true)}>
                <li> <span> Invoice type </span> <span>{Data?.invoiceType}</span> </li>
                <li> <span> Ｍobile barcode </span> <span> {Data?.invoiceCode} </span> </li>
              </ul>
            </div>
          </div>
          <div className="w-full px-4 flex-1">
            {/* Right Block  */}

            <div className="application-info-box">
              <div className="activity-details-custom-btn">
                <h2>Activity details</h2>
                <div>
                  <button
                    style={{
                      // display: "flex",
                      //color: "#1890FF",
                      backgroundColor: Data?.activityDetails[1]?.activityDetailsCustomerStatus !== "rejected" ? "#1890FF" : "white",
                      color: "white",
                      marginRight: 10,
                      borderWidth: "1px",
                      borderColor: Data?.activityDetails[1]?.activityDetailsCustomerStatus !== "rejected" ? "#1890FF" : "white",
                    }}
                    onClick={() => `${Data?.activityDetails?.length === 3 ? activetedService() : Data?.activityDetails?.length === 2 ? yesService() : approveService()}`}
                  >
                    {`${Data?.activityDetails?.length === 3 ? "Activate Service" : Data?.activityDetails?.length === 2 && Data?.activityDetails[1]?.activityDetailsCustomerStatus !== "rejected" ? "Yes" : Data?.activityDetails[1]?.activityDetailsCustomerStatus !== "rejected" ? "Approve" : ""}`}
                  </button>
                  {Data?.activityDetails?.length === 1 && <button
                    style={{
                      // display: "flex",
                      color: "red",
                      backgroundColor: "white",
                      borderColor: "red",
                      borderWidth: "2px",
                      // textDecorationLine: "underline",
                    }}
                    onClick={() => {
                      setRejectOpen(true);
                    }}
                  >
                    {console.log("here in the changes")}
                    {`${Data?.activityDetails?.length === 1 ? "Reject" : null}`}
                  </button>}
                </div>
              </div>
              {Data?.activityDetails?.reverse()?.map((item) => {
                console.log("here")
                return(
                <ul>
                  <li> <span> {`${item?.activityDetailsCustomerStatus === "unReviewed" ? "Is the application correct?" : item?.activityDetailsCustomerStatus === "pendingSmartMeterInstallation" ? "Did TaiPower install the same meter?" : "Did TaiPower approve the transfer agreement?"}`} </span></li>
                  <li> <span> Status : </span> <span> <strong>{item?.activityDetailsCustomerStatus}</strong> </span> </li>
                  <li> <span> {item?.activityDetailsUpdatedat} </span></li>
                </ul>
              )})}
            </div>
            {console.log("activity legth ------>", Data?.activityDetails)}
          </div>
          <div className="w-full"></div>

        </div>
      </div>

      {/* All Popups */}
      {taxOpen && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header">
                    <b>Contact TaxId</b>
                  </div>
                  <hr></hr>
                  <div className="form-group">
                    <label> Currunt Contact TaxId </label>
                    <strong>{Data?.taxId}</strong>
                  </div>
                  <div className="form-group">
                    <label> New Contact TaxId </label>
                    <input type="text" onChange={(e) => setTaxChange(e.target.value)}></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer">
                  <button className="cancel-btn" onClick={() => setTaxOpen(false)}
                  >
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditTax("userEmail", taxChange)
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setTaxOpen(false)}
        />
      )}
      {emailOpen && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header">
                    <b>Contact Email</b>
                  </div>
                  <hr></hr>
                  <div className="form-group">
                    <label> Currunt Contact Email </label>
                    <strong>{Data?.userEmail}</strong>
                  </div>
                  <div className="form-group">
                    <label> New Contact Email </label>
                    <input type="text" onChange={(e) => setEmailChange(e.target.value)}></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer">
                  <button className="cancel-btn" onClick={() => setEmailOpen(false)}
                  >
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditEmail("userEmail", emailChange)
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setEmailOpen(false)}
        />
      )}
      {signOpen && (
        <Popup

          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>E-Sign</b></div>
                  <hr></hr>
                </div>
                <div style={{border: "solid 1px #000000", borderStyle: 'dashed', padding: 10, display: "flex", justifyContent: "center"}}>
                  <img 
                    src="https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350"
                    alt="new"
                    style={{height: 220, width: 350}}
                  />
                </div>
              </div>
            </>
          }
          handleClose={() => setSignOpen(false)}
        />
      )}
      {householdName && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"> <b>Household name</b></div>
                  <hr></hr>
                  <div className="form-group">
                    <label>
                      Currunt Contact Email
                    </label>
                    <p>
                      {Data?.taipowerAccountName}
                    </p>
                  </div>
                  <div className="form-group">
                    <label>
                      New Household name
                    </label>
                    <input type="text"
                      onChange={(e) =>
                        setHouseholdNameChange(e.target.value)
                      }
                    ></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer" >
                  <button className="cancel-btn" onClick={() => setHouseholdName(false)}>
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditHousehold(
                      "taipowerAccountName",
                      householdNameChange
                    )
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setHouseholdName(false)}
        />
      )}
      {taipowerOpen && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>TaiPower account number</b></div>
                  <hr></hr>
                  <div className="form-group">
                    <label style={{ marginTop: 40, color: "#595959" }}>
                      Currunt TaiPower account number
                    </label>
                    <p>{Data?.userEmail}</p>
                  </div>
                  <div className="form-group">
                    <label>
                      New TaiPower account number
                    </label>

                    <input type="text"
                      onChange={(e) =>
                        setTaiAccounntChanges(e.target.value)
                      }
                    ></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer" >
                  <button className="cancel-btn" onClick={() => setTaiPowerOpen(false)} >
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditTaiAccountNumber(
                      "taipowerAccountNumber",
                      taiAccountChages
                    )
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setTaiPowerOpen(false)}
        />
      )}
      {primaryOpen && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Pirmary ID</b></div>
                  <hr></hr>
                  <div className="form-group">
                    <label>
                      Currunt Pirmary ID
                    </label>
                    <p>{Data?.userEmail}</p>
                  </div>
                  <div className="form-group">
                    <label>
                      New Pirmary ID
                    </label>

                    <input
                      type="text"
                      onChange={(e) => setPrimaryIdChange(e.target.value)}
                    ></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer">
                  <button className="cancel-btn" onClick={() => setPrimaryOpen(false)} >
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditPrimaryId("PrimaryId", primaryIdChange)
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setPrimaryOpen(false)}
        />
      )}
      {taiAddressOpen && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>TaiPower account number address</b></div>
                  <hr></hr>

                  <div className="form-group">
                    <label>
                      Currunt TaiPower account number address
                    </label>
                    <p>
                      {Data?.taipowerAccountName}
                    </p>
                  </div>
                  <div className="form-group">
                    <label>
                      New TaiPower account number address
                    </label>
                    <input
                      type="text"
                      onChange={(e) => setTaiAddressChanges(e.target.value)}
                    ></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer" >
                  <button className="cancel-btn" onClick={() => setTaiAddressOpen(false)}>
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditTaiAddress(
                      "taiadddress",
                      taiAddressChanges
                    )
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setTaiAddressOpen(false)}
        />
      )}
      {mailingOpen && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Mailing address</b></div>
                  <hr></hr>
                  <div className="form-group">
                    <label>
                      Currunt Mailing address
                    </label>
                    <p>{Data?.userEmail}</p>
                  </div>
                  <div className="form-group">
                    <label>
                      New Mailing address
                    </label>

                    <input
                      type="text"
                      onChange={(e) =>
                        setMailingAddressChange(e.target.value)
                      }
                    ></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer" >
                  <button className="cancel-btn" onClick={() => setMailingOpen(false)}>
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditMailingAddress(
                      "mailingAddress",
                      mailingAddressChange
                    )
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setMailingOpen(false)}
        />
      )}
      {RejectOpen && (
        <Popup
          content={
            <>
              <div className="popup-body">
                  <div className="popup-header"><b>Rejected reason</b></div>
                  <hr></hr>
                  <div className="form-group">
                  <div style={{display: "flex", marginTop: 15}}>
                    <input
                    type="checkbox"
                    value="Household name"
                    onChange={handleInputChange}
                  />
                  <label>&nbsp;Household name</label>
                </div>
                <div style={{display: "flex", marginTop: 15}}>
                <input
                  type="checkbox"
                  value="TaiPower account number"
                  onChange={handleInputChange}
                />
                <label>&nbsp;TaiPower account number</label>
                </div>
                <div style={{display: "flex", marginTop: 15}}>
                <input
                  type="checkbox"
                  value="Primary ID"
                  onChange={handleInputChange}
                />
                <label>&nbsp;Primary ID</label>
                </div>
                <div style={{display: "flex", marginTop: 15}}>
                <input
                  type="checkbox"
                  value="TaiPower account number address"
                  onChange={handleInputChange}
                />
                <label>&nbsp;TaiPower account number address</label>
                </div>
                <div style={{display: "flex", marginTop: 15, marginBottom: 15}}>
                <input
                  type="checkbox"
                  value="E-Sign"
                  onChange={handleInputChange}
                />
                <label>&nbsp;Esign</label>
                </div>
                <textarea
                  id="w3review"
                 
                  rows="3"
                  onChange={(e)=>{
                    setTextArea(e.target.value)
                  }}
                  placeholder="Input reject reason to applicant"
                  cols="40"
                />
                  </div>
                  <hr></hr>
                <div className="popup-footer" >
                  <button className="cancel-btn" onClick={() => setMailingOpen(false)}>
                    Cancel
                  </button>
                  <button className="save-btn" onClick={onFinal}
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setRejectOpen(false)}
        />
      )}

{payment && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Payment</b></div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      Credit card (Recurring)
                    </label>
                    <p>{`**** **** **** ${Data?.paymentDetails}`}</p>
                  </div>
                </div>
              </div>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Payment</b></div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      CVS
                    </label>
                    <p>{`-`}</p>
                  </div>
                </div>
              </div>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Payment</b></div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      ATM
                    </label>
                    <p>{`-`}</p>
                  </div>
                </div>
              </div>
            </>
          }
          handleClose={() => setPayment(false)}
        />
      )}

{invoice && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Invoice</b></div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      Invoice type
                    </label>
                    <p>{`E-iinvoice`}</p>
                  </div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      Tax Id
                    </label>
                    <p>{`${Data?.taxId ? Data?.taxId : '-'}`}</p>
                  </div>
                </div>
              </div>
              <div className="popup-body">
                  <div className="popup-header"><b>Invoice</b></div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      Invoice type
                    </label>
                    <p>{Data?.invoiceType}</p>
                  </div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      Charity Unit
                    </label>
                    <p>{Data?.invoiceCode}</p>
                  </div>
                </div>
            </>
          }
          handleClose={() => setInvoice(false)}
        />
      )}
    </div>
  );
}
