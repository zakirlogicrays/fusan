
import React from "react";
import "./../../assets/styles/popup.css";


const Popup = props => {
  return (
    <div className="popup-box">
      <div className="popup-main">
        <div className="box">
          <span className="close-icon" onClick={props.handleClose}><i class="fas fa-times"></i></span>
          {props.content}
        </div>
      </div>
    </div>
  );
};

export default Popup;