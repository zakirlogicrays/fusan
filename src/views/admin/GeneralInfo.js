
//import { borderColor, flex } from "tailwindcss/defaultTheme";
import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import { useParams } from "react-router";
import { popper } from "@popperjs/core";
import Popup from "./Popup";
import { height } from "tailwindcss/defaultTheme";
import { API_URL } from "APIURL";
import Bills from "./Bills";

export default function GeneralInfo() {

  const [Data, SetData] = useState();
  const [isOpen, setIsOpen] = useState(false);
  const [emailOpen, setEmailOpen] = useState(false);
  const [signOpen, setSignOpen] = useState(false);
  const [householdName, setHouseholdName] = useState(false);
  const [taipowerOpen, setTaiPowerOpen] = useState(false);
  const [primaryOpen, setPrimaryOpen] = useState(false);
  const [taiAddressOpen, setTaiAddressOpen] = useState(false);
  const [mailingOpen, setMailingOpen] = useState(false);
  const [taxOpen, setTaxOpen] = useState(false);
  const [taxChange, setTaxChange] = useState(undefined);
  const [emailChange, setEmailChange] = useState(undefined);
  const [householdNameChange, setHouseholdNameChange] = useState(undefined);
  const [taiAccountChages, setTaiAccounntChanges] = useState(undefined);
  const [primaryIdChange, setPrimaryIdChange] = useState(undefined);
  const [taiAddressChanges, setTaiAddressChanges] = useState(undefined);
  const [payment, setPayment] = useState(false);
  const [invoice, setInvoice] = useState(false);
  
  const [mailingAddressChange, setMailingAddressChange] = useState(undefined);
  const [button, setButton] = useState("generalInfo");
  const { id } = useParams();
  console.log("data ---->", Data)
  //console.log(id);
  // Pass useEffect a function
  useEffect(() => {
    let config = {
      headers: {
        Authorization: localStorage.getItem("tokendata"),
      },
    };
    axios
      .get(
        `${API_URL}admin/activatedusers/${id}`,
        config
      )

      .then(async function (response) {
        // handle success
        console.log("API", response.data);
        await SetData(response.data);
        console.log(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });

    //return () => console.log('unmounting...');
  }, []);

  const handleEditEmail = (lable, value) => {
    console.log("lable ---->", lable, value);
    axios({
      method: "put",
      url: `${API_URL}admin/activatedusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: { userEmail: value },
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/reviewusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setEmailOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditHousehold = (lable, value) => {
    console.log("lable ---->", lable, value);
    axios({
      method: "put",
      url: `${API_URL}admin/activatedusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: { taipowerAccountName: value },
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/reviewusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setHouseholdName(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditTaiAccountNumber = (lable, value) => {
    let stringValue = value.toString();
    axios({
      method: "put",
      url: `${API_URL}admin/activatedusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: { taipowerAccountNumber: stringValue },
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/activatedusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setTaiPowerOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditPrimaryId = (lable, value) => {
    console.log("lable ---->", lable, value);
    axios({
      method: "put",
      url: `${API_URL}admin/activatedusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: {"customerId":{
        "customerIdType": 1,
        "customerIdValue": value}},
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/activatedusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setPrimaryOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditTaiAddress = (lable, value) => {
    console.log("lable ---->", lable, value);
    axios({
      method: "put",
      url: `${API_URL}admin/activatedusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: { userAddress: value },
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/reviewusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setTaiAddressOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditMailingAddress = (lable, value) => {
    console.log("lable ---->", lable, value);
    axios({
      method: "put",
      url: `${API_URL}admin/activatedusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: { billingAddress: value },
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/activatedusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setMailingOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  };

  const handleEditTax = (lable, value) => {
    console.log("value ----->", value);
    axios({
      method: "put",
      url: `${API_URL}admin/reviewusers/${id}`,
      headers: { Authorization: localStorage.getItem("tokendata") },
      data: {"customerId":{
        "customerIdType": 2,
        "customerIdValue": value}}
    }).then((res) => {
      let config = {
        headers: {
          Authorization: localStorage.getItem("tokendata"),
        },
      };
      axios
        .get(`${API_URL}admin/reviewusers/${id}`, config)

        .then(async function (response) {
          // handle success
          console.log("API", response.data);
          await SetData(response.data);
          setTaxOpen(false);
          console.log(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    });
  }


  return (
    <div className="general-inf-main">
      <div className="title-main breadcrum">
        <h1>Activated Customers  <span>顏健康</span></h1>
      </div>
      <div style={{paddingBottom: 20}} className="border-bottom">
        <a style={{fontWeight: 700, color: button === "generalInfo" ? "#1890FF" : null, cursor: "pointer", borderBottom: button === "generalInfo" ? "solid 3px #1890FF" : null}} onClick={() => setButton("generalInfo")}>General info</a>
        <a style={{marginLeft: 20, fontWeight: 700, color: button === "bills" ? "#1890FF" : null, cursor: "pointer", borderBottom: button === "bills" ? "solid 3px #1890FF" : null}} onClick={() => setButton("bills")}>Bills</a>
      </div>

      {button === "generalInfo" && 
       <div className="w-full">
        <div className="flex flex-wrap">
          <div className="w-full px-4 flex-1">
            <div>
              {/* First block */}
            <div className="application-info-box">
              <h2>Customers info</h2>
              <ul>
              <li> <span> Date of Application </span> <span> {Data?.createdAt} </span> </li>
                <li> <span> Status </span> <span> <span> {Data?.customerStatus} </span> </span> </li>
                <li> <span> Contact Name </span> <span> {Data?.userName} </span> </li>
                <li> <span> Contact Email </span> <span> {Data?.userEmail} <button className="btn" onClick={() => { setEmailOpen(true); }}>Edit</button> </span> </li>
                <li> <span> Contact Mobile Phone </span> <span> {Data?.userPhone} </span> </li>
                <li> <span> E-sign </span> <span> <button className="btn" onClick={() => { setSignOpen(true); }}>View</button> </span> </li>
              </ul>
            </div>
            {/* Third block */}
            <div className="application-info-box">
                <h2>TaiPower info</h2>
                <ul>
                <li> <span> Household Name </span> <span> {Data?.taipowerAccountName} <button className="btn" onClick={() => setHouseholdName(true)}> Edit </button> </span> </li>
                <li> <span> TaiPower account number </span> <span> {Data?.taipowerAccountNumber} <button className="btn" onClick={() => setTaiPowerOpen(true)}> Edit </button> </span> </li>
                <li> <span> Pirmary ID </span> <span> {Data?.customerId} <button className="btn" onClick={() => setPrimaryOpen(true)}> Edit </button> </span> </li>
                <li> <span> Tax ID </span> <span> {Data?.taxId}  <button className="btn" onClick={() => setTaxOpen(true)}> Edit </button> </span> </li>
                <li> <span> TaiPower account number address </span> <span> {Data?.userAddress} <button className="btn" onClick={() => setTaiAddressOpen(true)}> Edit </button> </span> </li>
                <li> <span> Mailing address </span> <span> {Data?.billingAddress}  <button className="btn" onClick={() => setMailingOpen(true)} > Edit </button></span> </li>
                </ul>
            </div>
            </div>
          </div>
          <div className="w-full px-4 flex-1">
          <div className="application-info-box">
              <h2>KiWi Plan</h2>
              <ul>
              <ul>
                <li> <span> KiWi’s Plan 35 kwh/month </span> <span> {Data?.kiwiplan} </span> </li>
                <li> <span> Promo / Referral code </span> <span> {Data?.promoCode} </span> </li>
                <li> <span> Contact Name </span> <span> {Data?.userName} </span> </li>
              </ul>
              </ul>
            </div>
            {/* Fourth Block   */}
            <div className="application-info-box">
              <h2>Payment</h2>
              <ul style={{cursor: "pointer"}} onClick={() => setPayment(true)}>
                <li> <span> Credit card (One-time) </span> <span> {`**** **** **** ${Data?.paymentDetails}`} </span> </li>
              </ul>
            </div>
            {/* Fifth Block   */}
            <div className="application-info-box">
              <h2>E-Invoice</h2>
              <ul style={{cursor: "pointer"}} onClick={() => setInvoice(true)}>
                <li> <span> Invoice type </span> <span>{Data?.invoiceType}</span> </li>
                <li> <span> Ｍobile barcode </span> <span> {Data?.invoiceCode} </span> </li>
              </ul>
            </div>
          </div>
          <div className="w-full"></div>
        </div>
      </div>}

      {/* ALL POPUPS */}
      {taxOpen && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header">
                    <b>Contact TaxId</b>
                  </div>
                  <hr></hr>
                  <div className="form-group">
                    <label> Currunt Contact TaxId </label>
                    <strong>{Data?.taxId}</strong>
                  </div>
                  <div className="form-group">
                    <label> New Contact TaxId </label>
                    <input type="text" onChange={(e) => setTaxChange(e.target.value)}></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer">
                  <button className="cancel-btn" onClick={() => setTaxOpen(false)}
                  >
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditTax("userEmail", taxChange)
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setTaxOpen(false)}
        />
      )}
      {emailOpen && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header">
                    <b>Contact Email</b>
                  </div>
                  <hr></hr>
                  <div className="form-group">
                    <label> Currunt Contact Email </label>
                    <strong>{Data?.userEmail}</strong>
                  </div>
                  <div className="form-group">
                    <label> New Contact Email </label>
                    <input type="text" onChange={(e) => setEmailChange(e.target.value)}></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer">
                  <button className="cancel-btn" onClick={() => setEmailOpen(false)}
                  >
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditEmail("userEmail", emailChange)
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setEmailOpen(false)}
        />
      )}
      {signOpen && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Sign</b></div>
                  <hr></hr>
                  <p>
                    Rounded corners!
                  </p>
                </div>
                <hr></hr>
                <div className="popup-footer" >
                  <button className="cancel-btn" onClick={() => setHouseholdName(false)}>
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditHousehold(
                      "taipowerAccountName",
                      householdNameChange
                    )
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setSignOpen(false)}
        />
      )}
      {householdName && (
        <Popup
          content={
            <>
               <div>
                <div className="popup-body">
                  <div className="popup-header"> <b>Household name</b></div>
                  <hr></hr>
                  <div className="form-group">
                    <label>
                      Currunt Contact Email
                    </label>
                    <p>
                      {Data?.taipowerAccountName}
                    </p>
                  </div>
                  <div className="form-group">
                    <label>
                      New Household name
                    </label>
                    <input type="text"
                      onChange={(e) =>
                        setHouseholdNameChange(e.target.value)
                      }
                    ></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer" >
                  <button className="cancel-btn" onClick={() => setHouseholdName(false)}>
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditHousehold(
                      "taipowerAccountName",
                      householdNameChange
                    )
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setHouseholdName(false)}
        />
      )}
      {taipowerOpen && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>TaiPower account number</b></div>
                  <hr></hr>
                  <div className="form-group">
                    <label style={{ marginTop: 40, color: "#595959" }}>
                      Currunt TaiPower account number
                    </label>
                    <p>{Data?.userEmail}</p>
                  </div>
                  <div className="form-group">
                    <label>
                      New TaiPower account number
                    </label>

                    <input type="text"
                      onChange={(e) =>
                        setTaiAccounntChanges(e.target.value)
                      }
                    ></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer" >
                  <button className="cancel-btn" onClick={() => setTaiPowerOpen(false)} >
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditTaiAccountNumber(
                      "taipowerAccountNumber",
                      taiAccountChages
                    )
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setTaiPowerOpen(false)}
        />
      )}
      {primaryOpen && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Pirmary ID</b></div>


                  <hr></hr>
                  <div className="form-group">
                    <label>
                      Currunt Pirmary ID
                    </label>
                    <p>{Data?.userEmail}</p>
                  </div>
                  <div className="form-group">
                    <label>
                      New Pirmary ID
                    </label>

                    <input
                      type="text"
                      onChange={(e) => setPrimaryIdChange(e.target.value)}
                    ></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer">
                  <button className="cancel-btn" onClick={() => setPrimaryOpen(false)} >
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditPrimaryId("PrimaryId", primaryIdChange)
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setPrimaryOpen(false)}
        />
      )}
      {taiAddressOpen && (
        <Popup
          content={
            <>
             <div>
                <div className="popup-body">
                  <div className="popup-header"><b>TaiPower account number address</b></div>
                  <hr></hr>

                  <div className="form-group">
                    <label>
                      Currunt TaiPower account number address
                    </label>
                    <p>
                      {Data?.taipowerAccountName}
                    </p>
                  </div>
                  <div className="form-group">
                    <label>
                      New TaiPower account number address
                    </label>
                    <input
                      type="text"
                      onChange={(e) => setTaiAddressChanges(e.target.value)}
                    ></input>
                  </div>
                </div>
                <hr></hr>
                <div className="popup-footer" >
                  <button className="cancel-btn" onClick={() => setTaiAddressOpen(false)}>
                    Cancel
                  </button>
                  <button className="save-btn" onClick={() =>
                    handleEditTaiAddress(
                      "taiadddress",
                      taiAddressChanges
                    )
                  }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setTaiAddressOpen(false)}
        />
      )}
      {mailingOpen && (
        <Popup
          content={
            <>
              <div>
                <b>Mailing address</b>
                <hr style={{ marginTop: 20 }}></hr>
                <label style={{ marginTop: 40, color: "#595959" }}>
                  Currunt Mailing address
                </label>
                <p style={{ marginTop: 0 }}>{Data?.userEmail}</p>
                <label
                  style={{
                    marginTop: 100,
                    color: "#595959",
                    margin: "0px 4px",
                  }}
                >
                  New Mailing address
                </label>
                <div>
                  <input
                    style={{ height: 5 }}
                    type="text"
                    onChange={(e) =>
                      setMailingAddressChange(e.target.value)
                    }
                  ></input>
                </div>

                <hr></hr>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    padding: "5px 16px",
                  }}
                >
                  <button
                    style={{ backgroundColor: "white" }}
                    onClick={() => setMailingOpen(false)}
                  >
                    Cancel
                  </button>
                  <button
                    style={{ backgroundColor: "#1890FF" }}
                    onClick={() =>
                      handleEditMailingAddress(
                        "mailingAddress",
                        mailingAddressChange
                      )
                    }
                  >
                    Save
                  </button>
                </div>
              </div>
            </>
          }
          handleClose={() => setMailingOpen(false)}
        />
      )}
      {payment && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Payment</b></div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      Credit card (Recurring)
                    </label>
                    <p>{`**** **** **** ${Data?.paymentDetails}`}</p>
                  </div>
                </div>
              </div>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Payment</b></div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      CVS
                    </label>
                    <p>{`-`}</p>
                  </div>
                </div>
              </div>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Payment</b></div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      ATM
                    </label>
                    <p>{`-`}</p>
                  </div>
                </div>
              </div>
            </>
          }
          handleClose={() => setPayment(false)}
        />
      )}
      {invoice && (
        <Popup
          content={
            <>
              <div>
                <div className="popup-body">
                  <div className="popup-header"><b>Invoice</b></div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      Invoice type
                    </label>
                    <p>{`E-iinvoice`}</p>
                  </div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      Tax Id
                    </label>
                    <p>{`${Data?.taxId ? Data?.taxId : '-'}`}</p>
                  </div>
                </div>
              </div>
              <div className="popup-body">
                  <div className="popup-header"><b>Invoice</b></div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      Invoice type
                    </label>
                    <p>{Data?.invoiceType}</p>
                  </div>
                  <hr></hr>
                  <div className="form-group" style={{display: "flex", justifyContent: "space-between"}}>
                    <label>
                      Charity Unit
                    </label>
                    <p>{Data?.invoiceCode}</p>
                  </div>
                </div>
            </>
          }
          handleClose={() => setInvoice(false)}
        />
      )}
      {button === "bills" && 
         <Bills getData = {Data}></Bills>
      }
    </div>
   

  
  );
}
