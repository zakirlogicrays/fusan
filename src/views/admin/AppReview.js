import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  RangeDatePicker,
  // SingleDatePicker,
} from "react-google-flight-datepicker";
import "react-google-flight-datepicker/dist/main.css";
import moment from "moment";
import axios from "axios";

import TableDropdown from "components/Dropdowns/TableDropdown.js";
import { flex } from "tailwindcss/defaultTheme";
//import { sort } from "prelude-ls";
import { API_URL } from "APIURL";
import { AUTH_TOKEN } from "APIURL";
import { Link } from "react-router-dom";
import { useParams } from "react-router";

export default function AppReview({ props, color }) {
  const { id } = useParams();
  const [usview, setUsView] = useState("10");
  const [usstatus, setUsStatus] = useState(id ? id : "all");
  const [ussorting, setUsSort] = useState("Date of application: newest to oldest");
  const [usstartDate, setUsStartDate] = useState("2020-12-25");
  const [usendDate, setUsendDate] = useState("2021-08-14");
  const [Data, SetData] = useState();
  const [allData, setAllData] = useState(undefined);
  const [current, setCurrent] = useState(1);
  const [totalPages, setTotalPages] = useState([]);
  const [start, setStart] = useState(0);
  const [end, setEnd] = useState(2);
  //let totalPages = [];
  console.log("totalPages ----->", id);
  useEffect(() => {
    FetchData();
  }, [usview, usstatus, ussorting, usstartDate, usendDate, current]);

  useEffect(async() => {
    let config = {
      headers: {
        Authorization: localStorage.getItem("tokendata"),
      },
    };
    await axios
      .get(`${API_URL}admin/reviewusers?startDate=${usstartDate}&endDate=${usendDate}&status=${usstatus}&rowlimit=${usview}&sortOrder=${ussorting === "Date of application: newest to oldest" ? 0 : 1}&requestedPage=${current}`, config)

      .then(async function (response) {
        for(var i=1; i<=response.data.totalPages; i++){
          setTotalPages(oldArray => [...oldArray, i]);
        }
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  },[])

  const FetchData = async () => {
    let config = {
      headers: {
        Authorization: localStorage.getItem("tokendata"),
      },
    };
    await axios
      .get(`${API_URL}admin/reviewusers?startDate=${usstartDate}&endDate=${usendDate}&status=${usstatus}&rowlimit=${usview}&sortOrder=${ussorting === "Date of application: newest to oldest" ? 0 : 1}&requestedPage=${current}`, config)

      .then(async function (response) {
        //console.log("response ------->", response.data.totalPages)
        await SetData(response.data.docs);
        await setAllData(response.data.docs);
        // for(var i=1; i<=response.data.totalPages; i++){
        //   // totalPages.push(i)
        //   setTotalPages(oldArray => [...oldArray, i]);
        // }
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });

  }

  const onDateChange = (date1, date2) => {
    setUsStartDate(moment(date1).format("YYYY-MM-DD"));
    setUsendDate(moment(date2).format("YYYY-MM-DD"))
  };

  const handleViewSet = async (value) => {
    setUsView(value)
  }

  const handleStausSet = async (value) => {
    setUsStatus(value);
  }

  const handleShortSet = async (value) => {
    setUsSort(value);
  }

  const handleSearch = async (value) => {
    let searchData = allData.filter((item) => (
      item.userName.slice(0, value.length).toLowerCase().toString() === value.toLowerCase().toString() ||
        item.userEmail.slice(0, value.length).toLowerCase().toString() === value.toLowerCase().toString() ||
        item.taipowerAccountName.slice(0, value.length).toLowerCase().toString() === value.toLowerCase().toString() ||
        item.id.slice(0, value.length).toLowerCase().toString() === value.toLowerCase().toString() ||
        item.customerStatus.slice(0, value.length).toLowerCase().toString() === value.toLowerCase().toString() ||
        item.createdAt.slice(0, value.length).toLowerCase().toString() === value.toLowerCase().toString()
        ? item : null
    ))
    value.length > 0 ? SetData(searchData) : SetData(allData);
  }

  function download_csv_file() {  
     var data = 'Name,Email,Household Name,Kivi Plan,Status,Date\n';  
    for (var i = 0; i < Data.length; i++) {
        data += Data[i].userName+','+Data[i].userEmail+','+Data[i].taipowerAccountName+','+Data[i].id+','+Data[i].customerStatus+','+Data[i].createdAt;
        data += '\n';
  }    
     var hiddenElement = document.createElement('a');  
     hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(data);  
     hiddenElement.target = '_blank'; 
     hiddenElement.download = 'ApplicationReview.csv';  
     hiddenElement.click();  
  }  


  return (
    <>
      <div className="title-main">
        <h1>Applications to Review</h1>
        <p>Update approximately every 1 mins. Data as of 4:05 pm</p>
      </div>
      <div
        className={
          "relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded " +
          (color === "light" ? "bg-white" : "bg-lightBlue-900 text-white")
        }
      >

        <form className="md:flex hidden flex-row flex-wrap items-center mr-1 table-top-filter">

          <div className="w-full">
            <div className="flex flex-wrap">
              <div className="w-full px-4 flex-1">
                <div className="filter-left">
                  <h2>12 Applications</h2>

                  <div className="items-stretch">
                    <input
                      type="text"
                      placeholder="Search by name/phone/email or TP information"
                      className="search-input"
                      onChange={(e) => handleSearch(e.target.value)}
                    />
                    <span className="search-icon">
                      <i className="fas fa-search"></i>
                    </span>
                  </div>
                  <div className="left-status">
                    <lable>Status :</lable>
                    <select value={usstatus} onChange={(e) => {
                      handleStausSet(e.target.value)
                    }}>
                      <option value="all">All</option>
                      <option value="unReviewed">Unreviewed</option>
                      <option value="reSubmitted">Resubmitted</option>
                      <option value="rejected">Rejected</option>
                      <option value="pendingSmartMeterInstallation">Pending Install</option>
                      <option value="pendingTransferAgreement">
                        Pending Transfer Agreement
                      </option>
                    </select>
                  </div>
                </div>
              </div>

              <div className="w-full px-4 flex-1">
                <div className="right-filter">
                  <div className="export-button">
                    <button
                      onClick={()=>{download_csv_file()}}
                    >
                      Export to Csv
                    </button>
                  </div>
                  <div className="view-and-sort-by-main">
                    <div className="view-main">
                      <lable>View :</lable>
                      <select value={usview} onChange={(e) => {
                        handleViewSet(e.target.value)
                      }}>
                        <option value="1">
                          1
                        </option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                      </select>
                    </div>
                    <div className="sort-main">
                      <lable>Sort by:</lable>
                      <select value={ussorting} onChange={(e) => handleShortSet(e.target.value)}>
                        <option value="Date of application: newest to oldest">
                          Date of application: newest to oldest
                        </option>
                        <option value="Date of application: oldest to newest">
                          Date of application: oldest to newest
                        </option>
                      </select>
                    </div>
                  </div>
                  <div className="custom-date-picker">
                    <label>Date of aplication period</label>
                    <RangeDatePicker
                      onChange={(startDate, endDate) =>
                        onDateChange(startDate, endDate)
                      }
                      startDate={new Date(usstartDate)}
                      endDate={new Date(usendDate)}
                    />
                  </div>
                </div>
              </div>
              <div className="w-full"></div>
            </div>
          </div>
        </form>
        <div className="rounded-t mb-0 px-4 py-3 border-0">
          <div className="flex flex-wrap items-center">
            <div className="relative w-full px-4 max-w-full flex-grow flex-1">
              <h3
                className={
                  "font-semibold text-lg " +
                  (color === "light" ? "text-blueGray-700" : "text-white")
                }
              >
                Applications to Review
              </h3>
            </div>
          </div>
        </div>
        <div className="block w-full overflow-x-auto">
          {/* Projects table */}
          <table className="items-center w-full bg-transparent border-collapse">
            <thead>
              <tr>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Name
                </th>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Email
                </th>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Household name
                </th>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  KiVi Plan
                </th>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Status
                </th>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Date of Application
                </th>
              </tr>
            </thead>
            <tbody>
              {Data?.map((item) => (
                <tr style={{ cursor: "pointer" }}>
                  <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left flex items-center">
                  <Link to={`/admin/confirm/${item.id}`}>
                    <span
                      className={
                        "ml-3 font-bold " +
                        +(color === "light" ? "text-blueGray-600" : "text-white")
                      }
                    >
                      {item.userName}
                    </span>
                    </Link>
                  </th>
                  <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                  <Link to={`/admin/confirm/${item.id}`}>
                    {item.userEmail}
                  </Link>
                  </td>
                  <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                  <Link to={`/admin/confirm/${item.id}`}>
                    {item.taipowerAccountName}
                  </Link>
                  </td>
                  <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                    <div className="flex">
                    <Link to={`/admin/confirm/${item.id}`}>
                      {item.id}
                    </Link>
                    </div>
                  </td>
                  <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                    <div className="flex items-center">
                    <Link to={`/admin/confirm/${item.id}`}>
                      <span className="mr-2">{item.customerStatus}</span>
                    </Link>
                    </div>
                  </td>
                  <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-right">
                  <Link to={`/admin/confirm/${item.id}`}>
                    {item.createdAt}
                  </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div className="custom-pagination">
          <div className="py-2">
            <nav className="block">
              <ul className="flex pl-0 rounded list-none flex-wrap">
                <li  onClick={() => {
                  if(start > 0){
                  setStart(start - 1);
                  setEnd(end - 1);
                  }
                }}>
                  <a href="#pablo" className= {0 != start ? "first:ml-0 text-xs font-semibold flex w-8 h-8 mx-1 p-0 items-center justify-center leading-tight relative border border-solid border-lightBlue-500 bg-white text-lightBlue-500" : "first:ml-0 text-xs font-semibold flex w-8 h-8 mx-1 p-0 items-center justify-center leading-tight relative border border-solid border-lightBlue-200 text-white bg-lightBlue-200"}>
                    <i className="fas fa-chevron-left -ml-px"></i>
                  </a>
                </li>
                {totalPages?.slice(start, end)?.map((item) => (
                  <li onClick={() => setCurrent(item)}>
                  <a href="#pablo" className = {current === item ? "first:ml-0 text-xs font-semibold flex w-8 h-8 mx-1 p-0  items-center justify-center leading-tight relative border border-solid border-lightBlue-500 text-white bg-lightBlue-500" :  "first:ml-0 text-xs font-semibold flex w-8 h-8 mx-1 p-0  items-center justify-center leading-tight relative border border-solid border-lightBlue-500 bg-white text-lightBlue-500"}>
                    {item}
                  </a>
                </li>
                ))}
                <li onClick={() => {
                  console.log("totalpage && end ---->", totalPages.length , end)
                  if(totalPages.length > end){
                  setStart(start + 1);
                  setEnd(end + 1);
                  }
                }}>
                  <a href="#pablo" className= {totalPages.length != end ? "first:ml-0 text-xs font-semibold flex w-8 h-8 mx-1 p-0  items-center justify-center leading-tight relative border border-solid border-lightBlue-500 bg-white text-lightBlue-500" : "first:ml-0 text-xs font-semibold flex w-8 h-8 mx-1 p-0 items-center justify-center leading-tight relative border border-solid border-lightBlue-200 text-white bg-lightBlue-200"}>
                    <i className="fas fa-chevron-right -mr-px"></i>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </>
  );
}

AppReview.defaultProps = {
  color: "light",
};

AppReview.propTypes = {
  color: PropTypes.oneOf(["light", "light"]),
};
