import React from "react";
import PropTypes from "prop-types";

const CardStats = (props) =>{

  return (
    <>
      <div className="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0" style={{cursor: "pointer"}}>
        <div className="flex-auto p-4">
          <div className="flex flex-wrap">
            <div className="relative w-full pr-4 max-w-full flex-grow flex-1">
              <h5 className="box-title" style={{textAlign:'center'}}>
                {props?.statSubtitle}
              </h5>
              <span className="box-valu" style={{alignItems:'center',justifyContent:'center',display:'flex'}}>
                {props?.statTitle}
              </span>
            </div>
        </div>
      </div>
      </div>
    </>
  );
}

export default CardStats;
