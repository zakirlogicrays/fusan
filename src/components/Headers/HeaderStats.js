import React from "react";
import jwt_decode from "jwt-decode";

// components

//import CardStats from "components/Cards/CardStats.js";

export default function HeaderStats() {
  var decoded = jwt_decode(localStorage.getItem("tokendata"));
  return (
    <>
      {/* Header */}
      <div className="header-section-main">
        {/* <img src={{}} alt="admin-image"/> */}
        <p>{decoded.name}</p>
      </div>
    </>
  );
}
