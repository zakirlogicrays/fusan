import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Admin from "layouts/Admin.js";
import Landing from "views/Landing.js";
import Profile from "views/Profile.js";
import Index from "views/Index.js";
import Login from "views/auth/Login";
import Dashboard from "views/admin/Dashboard";
import ConfirmApplication from "views/admin/ConfrimApplication";
import Register from "views/auth/Register.js";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "assets/styles/tailwind.css";
import "assets/styles/style.css";
import Sidebar from "components/Sidebar/Sidebar";

let HideHeader = window.location.pathname === '/' && '/auth/register'  ? null : <Sidebar/>
ReactDOM.render(
  
  <BrowserRouter>
   {HideHeader}
    <Switch>
      <Route path="/admin" component={Admin} />
      <Route path="/admin/dashboard" exact component={Dashboard} />
      <Route path="/auth/register" exact component={Register} />
      <Route path="/landing" exact component={Landing} />
      <Route path="/profile" exact component={Profile} />
      <Route path="/" exact component={Login} />
      <Redirect from="*" to="/" />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);