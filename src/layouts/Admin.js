import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

// components

// import AdminNavbar from "components/Navbars/AdminNavbar.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import HeaderStats from "components/Headers/HeaderStats.js";
// import FooterAdmin from "components/Footers/FooterAdmin.js";

// views

import Dashboard from "views/admin/Dashboard.js";
// import Maps from "views/admin/Maps.js";
import AppReview from "views/admin/AppReview";
import ActivatedCustomer from "views/admin/ActivatedCustomer";
// import Testing from "views/admin/Testing";
import ConfrimApplication from "views/admin/ConfrimApplication";
import GeneralInfo from "views/admin/GeneralInfo";

export default function Admin() {
  return (
    <>
      <Sidebar />
      <div className="main-section relative md:ml-64 bg-blueGray-100">
        {/* <AdminNavbar /> */}
        {/* Header */}
        <HeaderStats />
        <div className="section-inner" >
          <Switch>
            <Route path="/admin/dashboard" exact component={Dashboard} />
            {/* <Route path="/admin/maps" exact component={Testing} /> */}
            <Route path="/admin/settings/:id" exact component={AppReview} />
            <Route path="/admin/activated-customer" exact component={ActivatedCustomer} />
            <Route path="/admin/activatedCustomer/:id" exact component={GeneralInfo} />
            <Route path="/admin/confirm/:id" exact component={ConfrimApplication} />
            <Route path="/admin/info" exact component={GeneralInfo} />

            <Redirect from="/admin" to="/admin/dashboard" />
          </Switch>
          {/* <FooterAdmin /> */}
        </div>
      </div>
    </>
  );
}
